from flask import Flask, Response, redirect, url_for, \
                                request, session, abort, render_template
import flask_login as flask_login
import base64
import datetime

def decode_token(token):
    d_str = base64.b64decode(token).split(',')
    return d_str[0]

app = Flask(__name__)

# config
app.config.update(
    DEBUG = True,
    SECRET_KEY = 'secret_xxx'
)

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
#login_manager.login_view = "login"

# mock user database
users = {
            'web01' : {'pw':'secret_password', 'name':'webb0x 01'},
            'web02' : {'pw':'s3cur3', 'name':'webb0x 02'},
            'web03' : {'pw':'95r<*M2f434b!4v', 'name':'webb0x 03'},
        }


class User(flask_login.UserMixin):
    pass
        
@login_manager.user_loader
def user_loader(username):
    if username not in users:
        return

    user = User()
    user.id = username
    user.name = users[username]['name']
    
    return user

@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    if username not in users:
        return

    user = User()
    user.id = username
    user.name = users[username]['name']

    user.is_authenticated = request.form['pw'] == users[username]['pw']

    return user


# the urls for the levels
@app.route('/')
def web01():
    msg = None
    msg_level = None
    next_url = "/"
    username = None
    lvl_auth = False
    if flask_login.current_user.is_authenticated:
        username = flask_login.current_user.name
        if flask_login.current_user.id == 'web01':
            lvl_auth = True
    if 'user_id' in session:
        msg = "Congratulations, you've completed level 01. Try out \
                    <a href=\"/level0x2\">level 02</a>?"
        msg_level = "success"
        username = session['user_id']
    return render_template("lvl01.html", title="level 01", next_url=next_url, 
                                msg=msg, alert_level=msg_level, 
                                lvl_auth=lvl_auth, user=username)

@app.route('/level0x2')
def web02():
    msg = None
    msg_level = None
    next_url = "/level0x2"
    username = None
    lvl_auth = False
    if flask_login.current_user.is_authenticated:
        username = flask_login.current_user.name
        if flask_login.current_user.id == 'web02':
            lvl_auth = True
    if 'user_id' in session and flask_login.current_user.id == 'web02':
        msg = "Congratulations, you've completed level 02. Try out \
                    <a href=\"/level0x03\">level 03</a>?"
        msg_level = "success"
        username = session['user_id']
    return render_template("lvl02.html", title="level 02", next_url=next_url, 
                                msg=msg, alert_level=msg_level, 
                                lvl_auth=lvl_auth, user=username)

@app.route('/level0x03')
def web03():
    msg = None
    msg_level = None
    next_url = "/level0x03"
    username = None
    lvl_auth = False
    if flask_login.current_user.is_authenticated:
        username = flask_login.current_user.name
        if flask_login.current_user.id == 'web03':
            lvl_auth = True
    if 'user_id' in session and flask_login.current_user.id == 'web03':
        msg = "Congratulations, you've completed level 03. Try out \
                    <a href=\"/level4\">level 04</a>?"
        msg_level = "success"
        username = session['user_id']
    return render_template("lvl03.html", title="level 03", next_url=next_url, 
                                msg=msg, alert_level=msg_level, 
                                lvl_auth=lvl_auth, user=username)
 
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        next_url = request.args.get('next')
        return render_template('login.html', next_url=next_url)

    if 'user_token' in request.cookies:
        # extract the cookie and see if the user should be logged in
        username = decode_token(request.cookies['user_token'])
        if username == 'web03':
            user = User()
            user.id = username
            user.name = users[username]['name']
            flask_login.login_user(user)
            return redirect("/level0x03")
        return 'Bad login'
        
    username = request.form['username']
    next_url = request.form['next']
    if username in users:
        if request.form['pw'] == users[username]['pw']:
            user = User()
            user.id = username
            user.name = users[username]['name']
            flask_login.login_user(user)
            return redirect(next_url)

    return 'Bad login'


# somewhere to logout
@app.route("/logout")
def logout():
    flask_login.logout_user()
    return Response('<p>Logged out</p>')


# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')
    

if __name__ == "__main__":
    app.run()