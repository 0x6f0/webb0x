webb0x
======

webb0x is a free, opensource and legal way to test your web hacking skills. It's been said that potential mates really dig things like that.

Documentation will be added soon, but for right now it's three levels that you can use to test your ability to exploit a website.

### Getting Started

  - First grab the code

  `git clone git@bitbucket.org:0x6f0/webb0x.git`

  - cd into the newly created directory

  `cd webb0x`

  - create a virtualenv, my personal style is

  `virtualenv .venv`

  - activate the environment

  `. .venv/bin/activate`

  - install the requirements

  `pip install -r requirements.txt`

  - launch the app

  `python app.py`

#### Have fun!